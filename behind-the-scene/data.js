import {RUPERT_MURDOCH, LVMH, CREDIT_AGRICOLE, GROUPE_SIPA_OUEST_FRANCE, DANIEL_KRETINSKY, GROUPE_LE_MONDE} from '../data_groups.js'

function getGroupWithPercent(group, percent){
  group['data']['percent'] = percent;
  return group;
}

const data = {
  "lemonde.fr":{
    "id": "lemonde",
    "data":{
      "name": "lemonde.fr",
     },
    "children": GROUPE_LE_MONDE['children']
  },
  'nouvelobs.com':{
    'id': 'nouvelobs',
    'data':{
      'name': 'nouvelobs.com',
      'link': 'https://fr.wikipedia.org/wiki/Le_Nouvel_Obs'
    },
    "children": GROUPE_LE_MONDE['children']
  },
  'leparisien.fr':{
    "id": "leparisien",
    "data":{
      "name": 'Le Parisien'
    },
    'children': LVMH['children']
  },
  'lesechos.fr':{
    'id': 'lesechos',
    'data':{
      'name': 'Les Echos'
    },
    'children':[{
      'id': 'groupelesechos',
      'data':{
        'name': 'Groupe Les Echos'
      },
      'children': LVMH['children']
    }]
  },
  'lefigaro.fr':{
    'id': 'lefigarofr',
    'data':{
      'name': 'lefigaro.fr'
    },
    'children':[{
      'id': 'groupefigaro',
      'data':{
        'name': 'Groupe Figaro'
      },
      'children':[{
        'id': 'groupedassault',
        'data':{
          'name': 'Groupe Dassault',
          'percent': 100
        },
      }]
    }]
  },
  'mediapart.fr':{
    'id': "mediapartfr",
    'data':{
      'name': "mediapart.fr",
    },
    'children':[{
      'id': "societeeditricedemediapart",
      'data':{
        'name': "Societe Editrice De Mediapart ",
        'percent': 100
      },
      'children':[{
        'id': "spim",
        'data':{
          'name': "Société pour la Protection de l’Indépendance de Mediapart - SPIM",
          'percent': 100
        },
        'children':[{
          'id': "fondpourunepresselibre",
          'data':{
            'name': "Fonds pour une presse libre",
            'percent': 100
          },
        }]
      }]
    }]
  },
  'next.ink':{
    'id': "nextimpact",
    'data':{
      'name': "Next Ink"
    },
    'children':[{
      'id': "impactmediagroup",
      'data':{
        'name': "INpact Mediagroup",
        'percent': 100
      },
      'children':[{
        'id': 'ouidothings',
        'data':{
          'name': 'OUI.DO/THINGS',
          'percent':76
        }
      }]
    }]
  },
  'lequipe.fr':{
    'id': "lequipefr",
    'data':{
      'name': "L'Equipe"
    },
    'children':[{
      'id': "Groupe Amaury",
      'data':{
        'name': "Groupe Amaury",
        'percent': 100
      },
      'children':[{
        'id':'',
        'data':{
          'name:': 'Famille Amaury',
          'percent': 100
        }
      }]
    }]
  },
  'facebook.com':{
    'id': "facebookcom",
    'data':{
      'name': "Facebook"
    },
    'children':[{
      'id': "metaplatforms",
      'data':{
        'name': "Meta Platforms",
        'percent': 100
      },
      'children':[
      {
        'id': "markzuckerberg",
        'data':{
          'name': "Mark Zuckerberg",
          'percent': 13.4,
          "link": "https://fr.wikipedia.org/wiki/Mark_Zuckerberg"
        },
      },
      {
        'id': "eduardosaverin",
        'data':{
          'name': "Eduardo Saverin",
          'percent': 2.1,
          "link": "https://fr.wikipedia.org/wiki/Eduardo_Saverin"
        },
      },
      {
        'id': "dustinmoskovitz",
        'data':{
          'name': "Dustin Moskovitz",
          'percent': 1.3,
          "link": "https://fr.wikipedia.org/wiki/Dustin_Moskovitz"
        },
      }
      ]
    }]
  },
  'google.com':{
    'id': "googlecom",
    'data':{
      'name': "Google.com"
    },
    'children':[{
      'id': "alphabet",
      'data':{
        'name': "Alphabet",
        'percent': 100
      },
      'children':[
        {
          'id': 'larrypage',
          'data':{
            'name': 'Larry Page',
            'percent': 26.3,
            'link': "https://fr.wikipedia.org/wiki/Larry_Page"
          }
        },
        {
          'id': 'EricSchmidt',
          'data':{
            'name': 'Eric Schmidt',
            'percent': 4.3,
            "link": "https://fr.wikipedia.org/wiki/Eric_Schmidt"
          }
        },
        {
          'id': 'SergeyBrin',
          'data':{
            'name': 'Sergey Brin',
            'percent': 25.3,
            'link': "https://fr.wikipedia.org/wiki/Sergey_Brin"
          }
        }
      ]
    }]
  },
  'amazon.com':{
    'id': "amazoncom",
    'data':{
      'name': "Amazon.com"
    },
    'children':[
      {
        'id': "jeffbezos",
        'data':{
          'name': "Jeff Bezos",
          'percent': 10,
          "link": "https://fr.wikipedia.org/wiki/Jeff_Bezos"
        },
      },
      {
        'id': "macenziebezos",
        'data':{
          'name': "MacKenzie Bezos",
          'percent': 3
        },
      },
      {
        'id': "thevanguardgroup",
        'data':{
          'name': "The Vanguard Group",
          'percent': 6.2,
          "link": "https://fr.wikipedia.org/wiki/The_Vanguard_Group"
        },
      },
      {
        'id': "blackrock",
        'data':{
          'name': "BlackRock",
          'percent': 5.2,
          "link": "https://fr.wikipedia.org/wiki/BlackRock"
        },
      },
    ]
  },
  'reddit.com':{
    'id': "redditcom",
    'data':{
      'name': "Reddit.com",
    },
    'children':[{
      'id': "advancepublications",
      'data':{
        'name': "Advance Publications",
        'percent': 100
      },
      'children':[{
        'id': "irvingfamily",
        'data':{
          'name': "Famille Samuel Irving Newhouse Sr.",
          'percent': 100
        }
      }]
    }]
  },
  'nytimes.com':{
    'id': "nytimescom",
    'data':{
      'name': "nytimes.com",
    },
    'children':[{
      'id': 'The New York Times',
      'data':{
        'name': "The New York Times",
        'percent': 100
      },
      'children':[{
        'id': "nytimescompany",
        'data':{
          'name': "The New York Times Company",
          'percent': 100
        },
        'children':[{
          'id': "FamilleOchsSulzberger",
          'data':{
            'name': "Famille Ochs-Sulzberger",
            'percent': 100
          },
        }]
      }]
    }]
  },
  'cnbc.com':{
    'id': "cbbccom",
    'data':{
      'name': "cnbc.com",
    },
    'children':[{
      'id': "CNBC",
      'data':{
        'name': "CNBC",
        'percent': 100
      },
      'children':[{
        'id': "NBCUniversal",
        'data':{
          'name': "NBCUniversal",
          'percent': 100
        },
        'children':[{
          'id': "ComcastCorporation",
          'data':{
            'name': "Comcast Corporation",
            'percent': 100
          },
          'children':[
            {
              'id': "thevanguardgroup",
              'data':{
                'name': "The Vanguard Group",
                'percent': 8,
                "link": "https://fr.wikipedia.org/wiki/The_Vanguard_Group"
              },
            },
            {
              'id': "SSgAFundsManagement",
              'data':{
                'name': "SSgA Funds Management",
                'percent': 3
              },
            },
            {
              'id': "blackrock",
              'data':{
                'name': "BlackRock Fund Advisors",
                'percent': 2,
                "link": "https://fr.wikipedia.org/wiki/BlackRock"
              },
            }
          ]
        }]
      }]
    }]
  },
  'foxnews.com':{
    'id': "foxnewscom",
    'data':{
      'name': "foxnews.com"
    },
    'children':[{
      'id': 'foxnewsmedia',
      'data':{
        'name': "Fox News Media"
      },
      'children':[{
        'id': 'foxcorp',
        'data':{
          'name': 'Fox Corporation'
        },
        'children':[
          {
            'id': 'Rupert Murdoch',
            'data':{
              'name': 'Rupert Murdoch',
              'percent': 40,
              "link": "https://fr.wikipedia.org/wiki/Rupert_Murdoch"
            }
          },
          {
            'id': 'Famille Murdoch',
            'data':{
              'name': 'Famille Murdoch',
              'percent': 39.6,
              "link": "https://fr.wikipedia.org/wiki/Cat%C3%A9gorie:Famille_Murdoch"
            }
          }
        ]
      }]
    }]
  },
  '20minutes.fr':{
    'id': "20minutesfr",
    'data':{
     'name': "20minutes.fr",
    },
    'children':[{
     'id': "GroupeRosselLaVoix ",
     'data':{
       'name': "Groupe Rossel La Voix ",
       'percent': 49.3,
       'link': "https://fr.wikipedia.org/wiki/Groupe_Rossel_La_Voix"
     },
     'children':[{
       'id': "GroupeRossel",
       'data':{
         'name': "Groupe Rossel",
         'percent': 73,
         'link': "https://fr.wikipedia.org/wiki/Groupe_Rossel"
       },
     },{
       'id': "creditagricolefr",
       'data':{
         'name': "Crédit Agricole Nord de France",
         'percent': 27
       },
       'children': CREDIT_AGRICOLE['children']
     }]
    },
    GROUPE_SIPA_OUEST_FRANCE]
  },
  'ouest-france.fr':{
    'id': "ouestfrancefr",
    'data':{
      'name': "ouest-france.fr",
    },
    'children':[{
     'id': "societeouestfrance",
     'data':{
       'name': "Société Ouest France",
       'percent': 100,
       'link': "https://fr.wikipedia.org/wiki/Ouest-France"
     },
     'children': [GROUPE_SIPA_OUEST_FRANCE]
    }]
  },
  'tf1.fr':{
    'id': 'tf1fr',
    'data':{
      'name': 'tf1.fr'
    },
    'children':[{
      'id': 'groupetf1',
      'data':{
        'name': 'Groupe TF1',
        'percent': 100,
        'link': 'https://fr.wikipedia.org/wiki/Groupe_TF1'
      },
      'children':[
        {
          'id': 'BouyguesSA',
          'data':{
            'name': 'Bouygues SA',
            'percent': 44.5,
            'link': "https://fr.wikipedia.org/wiki/Bouygues"
          },
          'children':[
            {
              'id': 'SCDM Holding',
              'data':{
                'name': 'SCDM Holding',
                'percent': 27.1
              }
            },
            {
              'id': 'Employees Bouygues S.A.',
              'data': {
                'name': 'Employees Bouygues S.A.',
                'percent': 21.3
              }
            }
          ]
        },{
          'id': 'salariePEStf1',
          'data':{
            'name': 'Employees Groupe TF1',
            'percent': 9.7
          }
        },
        {
          'id': 'Harris Associates LP',
          'data': {
            'name': 'Harris Associates LP',
            'percent': 6.22,
            'link': "https://en.wikipedia.org/wiki/Harris_Associates"
          }
        },{
          'id': 'Vesa Equity Investment SARL',
          'data':{
            'name': 'Vesa Equity Investment SARL',
            'percent': 5.05
          },
          'children':[DANIEL_KRETINSKY]
        },
        {
          'id': 'Azimut Capital Management SGR SpA',
          'data':{
            'name': 'Azimut Capital Management SGR SpA',
            'percent': 2.77,
            'link': 'https://en.wikipedia.org/wiki/Azimut_Holding'
          }
        }
      ]
    }]
  },
  'indiatimes.com':{
    'id': 'indiatimes',
    'data': {
      'name': 'India Times',
      'link': 'https://fr.wikipedia.org/wiki/The_Times_of_India'
    },
    'children':[{
      'id':'The_Times_Group',
      'data':{
        'name': 'The Times Group',
        'link': 'https://fr.wikipedia.org/wiki/The_Times_Group'
      },
      'children':[{
        'id': 'sahuJainFamily',
        'data': {
          'name': 'The Sahu Jain family',
          'link': 'https://en.wikipedia.org/wiki/Sahu_Jain_family'
        }
      }]
    }]
  },
  'yomiuri.co.jp':{
    'id': "Yomiuri shinbun",
    'data':{
      'name': "Yomiuri shinbun",
      'link': "https://fr.wikipedia.org/wiki/Yomiuri_shinbun"
    },
    'children':[{
      'id': "Matsutaro Shoriki",
      'data':{
        'name': 'Matsutarō Shōriki',
        'link': "https://fr.wikipedia.org/wiki/Matsutar%C5%8D_Sh%C5%8Driki",
        'percent': 100
      },
    }]
  },
  'wsj.com':{
    'id': "wsj",
    'data':{
      'name': "The Wall Street journal",
      'link': "https://fr.wikipedia.org/wiki/The_Wall_Street_Journal"
    },
    'children':[{
      'id': "News Corp",
      'data':{
        'name': "News Corp",
        'percent': 100,
        'link': 'https://fr.wikipedia.org/wiki/News_Corp'
      },
      'children':[
        getGroupWithPercent(RUPERT_MURDOCH, 100),
      ]
    }]
  },
  'breitbart.com':{
    'id': "Breitbart",
    'data':{
      'name': "Breitbart",
      'link': ''
    },
    'children':[{
        'id': "Breitbart News Network",
        'data':{
          'name': "Breitbart News Network",
          'percent': 100,
          'link': 'https://fr.wikipedia.org/wiki/Breitbart_News'
        },
        'children':[
          {
            'id': "Larry_Solov",
            'data':{
              'name': "Larry Solov",
              'link': 'https://fr.wikipedia.org/wiki/Larry_Solov',
              'description': "<ul><li>Far-right nationalist</li></ul>"
            }
          },
          {
            'id': "Steve_Bannon",
            'data':{
              'name': "Steve Bannon",
              'link': "https://fr.wikipedia.org/wiki/Steve_Bannon",
              'description': "<ul><li>Far-right nationalist</li>\
                                  <li>anti-Europe</li>\
                              </ul>"
            }
          },
          {
            'id': "Susie Breitbart",
            'data':{
              'name': "Susie Breitbart",
              'link': ""
            }
          },
          {
            'id': "MercersFamily",
            'data':{
              'name': "Mercers family",
              'link': "https://en.wikipedia.org/wiki/Robert_Mercer",
              'description': "<ul><li>Far-right</li>\
                                  <li>anti-Europe</li>\
                              </ul>"
            }
          }

        ]
      }
    ]
  }
};

export default data;
