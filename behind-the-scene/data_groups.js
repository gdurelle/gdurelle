// Basic architecture
//
//   'id': "something",
//   'data':{
//     'name': "loremipsum",
//     'link': ''
//   },
//   'children':[{
//     'id': "something",
//     'data':{
//       'name': "loremipsum",
//       'percent': 100,
//       'link': ''
//     },
//   }]
//

const RUPERT_MURDOCH = {
  'id': 'Rupert Murdoch',
  'data':{
    'name': 'Rupert Murdoch',
    "link": "https://fr.wikipedia.org/wiki/Rupert_Murdoch",
    "description": "<ul><li>Climate Change Denial</li>\
                    <li>Euroscepticism</li></ul>"
  }
}

// shortcut: 'children': LVMH['children']
const LVMH = {
  "children":[{
    'id': 'lvmh',
    'data':{
      'name': 'LVMH'
    },
    'children': [{
      'id': 'bernard',
      'data':{
        'name': 'Bernard Arnault',
        'percent': 47,
        "link": "https://fr.wikipedia.org/wiki/Bernard_Arnault"
      }
    }]
  }]
};

// shortcut: 'children': CREDIT_AGRICOLE['children']
const CREDIT_AGRICOLE = {
  'children':[{
   'id': 'creditagricolesa',
   'data':{
     'name': 'Crédit Agricole S.A.',
     'percent': 100
    },
   'children':[{
     'id': 'sasruedelaboetie',
     'data':{
       'name': "SAS Rue la Boétie",
       'percent': 55.5,
       'link': "https://fr.wikipedia.org/wiki/SAS_Rue_La_Bo%C3%A9tie"
     },
     'children':[{
       'id': 'federatNationalCrediAgricole',
       'data':{
         'name': 'Fédération nationale du Crédit agricole'
       }
     }]
    },{
     'id': 'creditagricolesaemployees',
     'data': {
       'name': 'Employees Crédit Agricole S.A. ',
       'percent': 5
     }
    },{
     'id': 'creditagricolesatshares',
     'data':{
       'name': 'Treasury shares Crédit Agricole S.A.',
       'percent': 2.8
      }
    }]
  }]
}

// shortcut: 'children': GROUPE_SIPA_OUEST_FRANCE['children']
const GROUPE_SIPA_OUEST_FRANCE = {
  'id': 'grooupesipaouestfrance',
  'data':{
    'name': 'Groupe Sipa Ouest France',
    'percent': 49.3,
    'link': "https://fr.wikipedia.org/wiki/Groupe_SIPA_Ouest-France"
  },
  'children':[{
    'id': 'assosoutprincipedemocrahumaniste',
    'data':{
       'name': "Association pour le soutien des principes de la démocratie humaniste",
       'percent': 99.99,
       'link': "https://fr.wikipedia.org/wiki/Association_pour_le_soutien_des_principes_de_la_d%C3%A9mocratie_humaniste"
    }
  }]
}

// shortcut: 'children': DANIEL_KRETINSKY['children']
const DANIEL_KRETINSKY = {
  "id": "Daniel Kretinsky",
  "data":{
    "name": "Daniel Kretinsky",
    "link": "https://en.wikipedia.org/wiki/Daniel_K%C5%99et%C3%ADnsk%C3%BD"
   }
}

const GROUPE_LE_MONDE = {
  "children": [
  {
    "id": "idB",
    "data":{
      "name": "Groupe Le Monde",
      'percent': 100
    },
    "children": [
    {
      "id": "idC",
      "data":{
        "name": "Le Monde Libre",
        "percent": 72.25
      },
      "children":[
         {
           "id": "xn",
           "data": {
             "name": "Xavier Niel",
             "link": "https://fr.wikipedia.org/wiki/Xavier_Niel",
             'percent': 26
           }
         },
         {
           "id": "lnm",
           "data":{
             "name": "Le Nouveau Monde",
             "percent": 26.6
            },
           "children":[
              {
                "id": "NJJ Presse",
                "data":{
                  "name": "NJJ Presse"
                 },
              },
              DANIEL_KRETINSKY,
              {
                "id": "Matthieu Pigasse",
                "data":{
                  "name": "Matthieu Pigasse",
                  "link": "https://fr.wikipedia.org/wiki/Matthieu_Pigasse"
                 },
              }
           ]
         },
         {
           "id": "bm",
           "data":{
             "name": "Berlys Media",
             "percent": 26.6
            },
         },
         {
           "id": "p",
           "data":{
             "name": "Prisa",
             "percent": 20,
             "link": "https://fr.wikipedia.org/wiki/Prisa"
            },
         }
      ]
    },
    {
       "id": "spoleindegroupelemonde",
       "data": {
         "name": "Pôle d'indépendance du Groupe Le Monde",
         "percent": 25.4,
         "link": "https://fr.wikipedia.org/w/index.php?title=P%C3%B4le_d%27ind%C3%A9pendance_du_Groupe_Le_Monde&oldid=217516027"
       }
    }
    ]
  }
  ]
}

export {RUPERT_MURDOCH, LVMH, CREDIT_AGRICOLE, GROUPE_SIPA_OUEST_FRANCE, DANIEL_KRETINSKY, GROUPE_LE_MONDE};
