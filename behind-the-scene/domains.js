import data from './data.js'

const domains = Object.keys(data);

document.addEventListener("DOMContentLoaded", ()=>{
  const domainList = document.getElementById('domainList');

  for(let domain of domains){
    var li = document.createElement('li');
    var a = document.createElement('a');
    a.setAttribute('target', '_blank');
    a.setAttribute('href', '//'+domain);
    a.textContent = domain;
    li.appendChild(a)
    domainList.appendChild(li);
  }
});
