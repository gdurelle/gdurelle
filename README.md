# README

## Depedencies

* https://fontawesome.com/download


## Setup

### Bundler
Vous avez besoins d'installer Bundler si ce n'est pas déjà fait.
```zsh
gem install bundler
```

### Jekyll
```zsh
bundle install
```

### Run
```zsh
bundle exec jekyll serve --livereload
```

# Deploy

1. ```JEKYLL_ENV=production bundle exec jekyll build```
2. ```scp -r -p ./_site/ ovhK:/srv/gdurelle.com/```

```scp -p ./_site/index.html ovhK:/srv/gdurelle.com/_site/index.html```
```scp -r -p ./_site/behind-the-scene/ ovhK:/srv/gdurelle.com/_site/```
```bash
scp -p ./_site/behind-the-scene/index.html ovhK:/srv/gdurelle.com/_site/behind-the-scene/index.html
```

```zsh
scp -p ./_site/behind-the-scene/data.js ovhK:/srv/gdurelle.com/_site/behind-the-scene/data.js
```


# Edition

English flag : 🇬🇧
French flag : 🇫🇷
