---
layout: posts
title:  "Kali Linux on macOS with VirtualBox"
date:   2021-10-20
categories: tech 
tags: 🇬🇧 osx vm linux
---

Just after upgrading my macOS to BigSur (11.6) my VirtualBox Linux stopped working.
```
VERR_VM_DRIVER_NOT_INSTALLED (-1908) - The support driver is not installed. On linux, open returned ENOENT. 
```
The fix is fortunately pretty simple.


## The fix
1. Uninstall Virtualbox
2. Reinstall VirtualBox to last version
3. ALSO install the ```Oracle VM VirtualBox Extension Pack```

You can find the packages here: [https://www.virtualbox.org/wiki/Downloads](https://www.virtualbox.org/wiki/Downloads)

## Explanation
When updating the OS, changes can sometimes reset some settings about space and memory management or disk drivers, breaking how VM does its virtualisation based on your OS settings in order to make it work.

This is the same error I had on the first install because nothing was in place yet.

I bet i will have the same error on next OS update.

## Quick notes
You could also get another message with:
```
NS_ERROR_FAILURE
```
