---
layout: posts
title:  "Puma & sidekiq sous Systemd"
date:   2022-10-16
categories: tech 
tags: 🇫🇷 linux sidekiq puma rails
---

Je ne vais pas vous refaire une explication de ce qu'est <i>systemd</i>.<br>
Simplement partager comment faire tourner un projet <strong>Rails</strong> (7.0) sous <strong>Puma 5</strong> et <strong>Sidekiq 6</strong>.

Je ne suis pas trop fan de <i>systemd</i>, je ne l'utilise que par obligation. Je ne suis pas sysadmin, et pour déployer un projet perso en ligne j'ai en gros le choix entre du metal et du cloud.
<br>
J'ai choisi le metal car ça me permet de faire ce que je veux sans contrainte et pour un coût moindre.

Initialement Puma se lançais au déploiement via <i>Capistrano</i>, en mode <i>daemon</i>, mais la version 5 à complètement supprimer cela.<br>
A tort ou à raison n'est pas le sujet ici, on va se concentrer sur ce qu'il faut faire.

1. Fichiers de services & socket
2. Recharger le loader systemd
3. Activer les fichiers
4. Démarrer les services

## Préambule
Pour chaque projet sur le serveur, je crée un user different.
Je me trouve donc dans un répertoire du même nom que mon appli: /home/monappli.

Pour lancer un service il y a 2 possibilitées:
- Soit en mode system
- Soit en mode user.
On lancera en mode user, cela permet de ne pas les lancer comme root, meilleur pour la sécurité.

Pour un service en mode user il faut placer les fichiers de service à un endroit précis de home user:
<i>\~/.config/systemd/user/</i>

En chemin absolu: <i>/home/monappli/.config/systemd/user/</i>

## Puma
### 1. Les fichiers de service
Une convention de nommage de ces fichiers est: monappli_leprogramme_env
Essayez de la respecter, ça vous facilitera la vie avec tout ce qui utilise ce nommage par defaut.

Ici mon application Rails s'appelle: monappli<br>
Nous somme sur un environnement nommé: staging<br>
Nous allons lancer: puma<br>

Le premier et plus important fichier est le fichier .service, donc <strong>monappli_puma_env.service</strong>
```
[Unit]
Description=Puma HTTP Server for monappli staging
After=network.target
Requires=monappli_puma_staging.socket

[Service]
Type=simple
Environment=RAILS_ENV=staging
WorkingDirectory=/srv/www/monappli/current/
ExecStart=/home/monappli/.rbenv/shims/puma -e staging -C /srv/www/monappli/shared/puma.rb /srv/www/monappli/current/config.ru
ExecStop=/home/monappli/.rbenv/shims/puma -e staging -C /srv/www/monappli/shared/puma.rb stop
ExecReload=/home/monappli/.rbenv/shims/puma -e staging -C /srv/www/monappli/shared/puma.rb phased-restart

PIDFile=/srv/www/monappli/shared/tmp/pids/puma.pid

Restart=always
RestartSec=8
KillMode=process
SyslogIdentifier=puma

[Install]
WantedBy=multi-user.target
```
#### Fichier de socket

```
[Unit]
Description=Puma HTTP Server Accept Sockets

[Socket]
ListenStream=0.0.0.0:9294
ListenStream=0.0.0.0:9295

NoDelay=true
ReusePort=true
Backlog=1024

[Install]
WantedBy=sockets.target
```
### 2. Recharger le loader de systemd
Passer en root:
```systemctl daemon-reload```

### Activer les fichier
```systemctl enable monappli_puma_env.service monappli_puma_env.socket```

### Démarrer les services
Repasser en user puis il suffit de taper le contenu de ExecStart, qui devrait marcher de lui-même.

Ou encore depuis votre console locale:
```cap puma:start```

## Sidekiq
Si vous rajoutez Sidekiq, c'est la même chose
```
[Unit]
Description=sidekiq for monappli
After=syslog.target network.target

[Service]
Type=simple
Environment=RAILS_ENV=staging
WorkingDirectory=/srv/www/monappli/current
ExecStart=/home/monappli/.rbenv/bin/rbenv exec bundle exec sidekiq -e staging
StandardOutput=append:/srv/www/monappli/shared/log/sidekiq.log
StandardError=append:/srv/www/monappli/shared/log/sidekiq.error.log

RestartSec=20
Restart=on-failure

SyslogIdentifier=sidekiq

[Install]
WantedBy=default.target
```
